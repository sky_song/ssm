package com.song.crud.service;

import java.util.List;
import com.song.crud.domain.Employee;

/**
 * 员工信息Service接口
 * 
 * @author song
 * @date 2021-03-24
 */
public interface IEmployeeService 
{
    /**
     * 查询员工信息
     * 
     * @param id 员工信息ID
     * @return 员工信息
     */
    public Employee selectEmployeeById(String id);

    /**
     * 查询员工信息列表
     * 
     * @param employee 员工信息
     * @return 员工信息集合
     */
    public List<Employee> selectEmployeeList(Employee employee);

    /**
     * 新增员工信息
     * 
     * @param employee 员工信息
     * @return 结果
     */
    public int insertEmployee(Employee employee);

    /**
     * 修改员工信息
     * 
     * @param employee 员工信息
     * @return 结果
     */
    public int updateEmployee(Employee employee);

    /**
     * 批量删除员工信息
     * 
     * @param ids 需要删除的员工信息ID
     * @return 结果
     */
    public int deleteEmployeeByIds(String[] ids);

    /**
     * 删除员工信息信息
     * 
     * @param id 员工信息ID
     * @return 结果
     */
    public int deleteEmployeeById(String id);
}
