package com.song.crud.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.song.crud.mapper.LoginInfoMapper;
import com.song.crud.domain.LoginInfo;
import com.song.crud.service.ILoginInfoService;

/**
 * 登录信息Service业务层处理
 *
 * @author song
 * @date 2021-03-24
 */
@Service
public class LoginInfoServiceImpl implements ILoginInfoService
{
    @Autowired
    private LoginInfoMapper loginInfoMapper;

    /**
     * 查询登录信息
     *
     * @param id 登录信息ID
     * @return 登录信息
     */
    @Override
    public LoginInfo selectLoginInfoById(String id)
    {
        return loginInfoMapper.selectLoginInfoById(id);
    }

    /**
     * 查询登录信息列表
     *
     * @param loginInfo 登录信息
     * @return 登录信息
     */
    @Override
    public List<LoginInfo> selectLoginInfoList(LoginInfo loginInfo)
    {
        return loginInfoMapper.selectLoginInfoList(loginInfo);
    }
    @Override
    public List<LoginInfo> LoginInfoAndemployResult(LoginInfo loginInfo)
    {
        return loginInfoMapper.LoginInfoAndemployResult(loginInfo);
    }

    /**
     * 新增登录信息
     *
     * @param loginInfo 登录信息
     * @return 结果
     */
    @Override
    public int insertLoginInfo(LoginInfo loginInfo)
    {
        return loginInfoMapper.insertLoginInfo(loginInfo);
    }

    /**
     * 修改登录信息
     *
     * @param loginInfo 登录信息
     * @return 结果
     */
    @Override
    public int updateLoginInfo(LoginInfo loginInfo)
    {
        return loginInfoMapper.updateLoginInfo(loginInfo);
    }

    /**
     * 批量删除登录信息
     *
     * @param ids 需要删除的登录信息ID
     * @return 结果
     */
    @Override
    public int deleteLoginInfoByIds(String[] ids)
    {
        return loginInfoMapper.deleteLoginInfoByIds(ids);
    }

    /**
     * 删除登录信息信息
     *
     * @param id 登录信息ID
     * @return 结果
     */
    @Override
    public int deleteLoginInfoById(String id)
    {
        return loginInfoMapper.deleteLoginInfoById(id);
    }
}
