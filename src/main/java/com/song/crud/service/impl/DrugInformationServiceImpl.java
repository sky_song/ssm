package com.song.crud.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.song.crud.mapper.DrugInformationMapper;
import com.song.crud.domain.DrugInformation;
import com.song.crud.service.IDrugInformationService;

/**
 * 药品信息Service业务层处理
 * 
 * @author 宋连华
 * @date 2021-04-07
 */
@Service
public class DrugInformationServiceImpl implements IDrugInformationService 
{
    @Autowired
    private DrugInformationMapper drugInformationMapper;

    /**
     * 查询药品信息
     * 
     * @param id 药品信息ID
     * @return 药品信息
     */
    @Override
    public DrugInformation selectDrugInformationById(Long id)
    {
        return drugInformationMapper.selectDrugInformationById(id);
    }

    /**
     * 查询药品信息列表
     * 
     * @param drugInformation 药品信息
     * @return 药品信息
     */
    @Override
    public List<DrugInformation> selectDrugInformationList(DrugInformation drugInformation)
    {
        return drugInformationMapper.selectDrugInformationList(drugInformation);
    }

    /**
     * 新增药品信息
     * 
     * @param drugInformation 药品信息
     * @return 结果
     */
    @Override
    public int insertDrugInformation(DrugInformation drugInformation)
    {
        return drugInformationMapper.insertDrugInformation(drugInformation);
    }

    /**
     * 修改药品信息
     * 
     * @param drugInformation 药品信息
     * @return 结果
     */
    @Override
    public int updateDrugInformation(DrugInformation drugInformation)
    {
        return drugInformationMapper.updateDrugInformation(drugInformation);
    }

    /**
     * 批量删除药品信息
     * 
     * @param ids 需要删除的药品信息ID
     * @return 结果
     */
    @Override
    public int deleteDrugInformationByIds(Long[] ids)
    {
        return drugInformationMapper.deleteDrugInformationByIds(ids);
    }

    /**
     * 删除药品信息信息
     * 
     * @param id 药品信息ID
     * @return 结果
     */
    @Override
    public int deleteDrugInformationById(Long id)
    {
        return drugInformationMapper.deleteDrugInformationById(id);
    }
}
