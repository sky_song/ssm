package com.song.crud.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.song.crud.mapper.EmployeeMapper;
import com.song.crud.domain.Employee;
import com.song.crud.service.IEmployeeService;

/**
 * 员工信息Service业务层处理
 * 
 * @author song
 * @date 2021-03-24
 */
@Service
public class EmployeeServiceImpl implements IEmployeeService 
{
    @Autowired
    private EmployeeMapper employeeMapper;

    /**
     * 查询员工信息
     * 
     * @param id 员工信息ID
     * @return 员工信息
     */
    @Override
    public Employee selectEmployeeById(String id)
    {
        return employeeMapper.selectEmployeeById(id);
    }

    /**
     * 查询员工信息列表
     * 
     * @param employee 员工信息
     * @return 员工信息
     */
    @Override
    public List<Employee> selectEmployeeList(Employee employee)
    {
        return employeeMapper.selectEmployeeList(employee);
    }

    /**
     * 新增员工信息
     * 
     * @param employee 员工信息
     * @return 结果
     */
    @Override
    public int insertEmployee(Employee employee)
    {
        return employeeMapper.insertEmployee(employee);
    }

    /**
     * 修改员工信息
     * 
     * @param employee 员工信息
     * @return 结果
     */
    @Override
    public int updateEmployee(Employee employee)
    {
        return employeeMapper.updateEmployee(employee);
    }

    /**
     * 批量删除员工信息
     * 
     * @param ids 需要删除的员工信息ID
     * @return 结果
     */
    @Override
    public int deleteEmployeeByIds(String[] ids)
    {
        return employeeMapper.deleteEmployeeByIds(ids);
    }

    /**
     * 删除员工信息信息
     * 
     * @param id 员工信息ID
     * @return 结果
     */
    @Override
    public int deleteEmployeeById(String id)
    {
        return employeeMapper.deleteEmployeeById(id);
    }
}
