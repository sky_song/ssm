package com.song.crud.service.impl;

import java.util.List;

import com.song.crud.common.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.song.crud.mapper.CirculationMapper;
import com.song.crud.domain.Circulation;
import com.song.crud.service.ICirculationService;

/**
 * 流通记录Service业务层处理
 *
 * @author 宋连华
 * @date 2021-04-07
 */
@Service
public class CirculationServiceImpl implements ICirculationService
{
    @Autowired
    private CirculationMapper circulationMapper;

    /**
     * 查询流通记录
     *
     * @param id 流通记录ID
     * @return 流通记录
     */
    @Override
    public Circulation selectCirculationById(Long id)
    {
        return circulationMapper.selectCirculationById(id);
    }

    /**
     * 查询流通记录列表
     *
     * @param circulation 流通记录
     * @return 流通记录
     */
    @Override
    public List<Circulation> selectCirculationList(Circulation circulation)
    {
        return circulationMapper.selectCirculationList(circulation);
    }

    /**
     * 新增流通记录
     *
     * @param circulation 流通记录
     * @return 结果
     */
    @Override
    public int insertCirculation(Circulation circulation)
    {
        circulation.setCreateTime(DateUtils.getNowDate());
        return circulationMapper.insertCirculation(circulation);
    }

    /**
     * 修改流通记录
     *
     * @param circulation 流通记录
     * @return 结果
     */
    @Override
    public int updateCirculation(Circulation circulation)
    {
        return circulationMapper.updateCirculation(circulation);
    }

    /**
     * 批量删除流通记录
     *
     * @param ids 需要删除的流通记录ID
     * @return 结果
     */
    @Override
    public int deleteCirculationByIds(Long[] ids)
    {
        return circulationMapper.deleteCirculationByIds(ids);
    }

    /**
     * 删除流通记录信息
     *
     * @param id 流通记录ID
     * @return 结果
     */
    @Override
    public int deleteCirculationById(Long id)
    {
        return circulationMapper.deleteCirculationById(id);
    }
}
