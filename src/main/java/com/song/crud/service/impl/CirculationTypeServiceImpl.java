package com.song.crud.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.song.crud.mapper.CirculationTypeMapper;
import com.song.crud.domain.CirculationType;
import com.song.crud.service.ICirculationTypeService;

/**
 * 流通记录Service业务层处理
 * 
 * @author 宋连华
 * @date 2021-04-07
 */
@Service
public class CirculationTypeServiceImpl implements ICirculationTypeService 
{
    @Autowired
    private CirculationTypeMapper circulationTypeMapper;

    /**
     * 查询流通记录
     * 
     * @param id 流通记录ID
     * @return 流通记录
     */
    @Override
    public CirculationType selectCirculationTypeById(Long id)
    {
        return circulationTypeMapper.selectCirculationTypeById(id);
    }

    /**
     * 查询流通记录列表
     * 
     * @param circulationType 流通记录
     * @return 流通记录
     */
    @Override
    public List<CirculationType> selectCirculationTypeList(CirculationType circulationType)
    {
        return circulationTypeMapper.selectCirculationTypeList(circulationType);
    }

    /**
     * 新增流通记录
     * 
     * @param circulationType 流通记录
     * @return 结果
     */
    @Override
    public int insertCirculationType(CirculationType circulationType)
    {
        return circulationTypeMapper.insertCirculationType(circulationType);
    }

    /**
     * 修改流通记录
     * 
     * @param circulationType 流通记录
     * @return 结果
     */
    @Override
    public int updateCirculationType(CirculationType circulationType)
    {
        return circulationTypeMapper.updateCirculationType(circulationType);
    }

    /**
     * 批量删除流通记录
     * 
     * @param ids 需要删除的流通记录ID
     * @return 结果
     */
    @Override
    public int deleteCirculationTypeByIds(Long[] ids)
    {
        return circulationTypeMapper.deleteCirculationTypeByIds(ids);
    }

    /**
     * 删除流通记录信息
     * 
     * @param id 流通记录ID
     * @return 结果
     */
    @Override
    public int deleteCirculationTypeById(Long id)
    {
        return circulationTypeMapper.deleteCirculationTypeById(id);
    }
}
