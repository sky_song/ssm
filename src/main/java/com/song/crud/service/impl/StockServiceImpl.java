package com.song.crud.service.impl;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.song.crud.mapper.StockMapper;
import com.song.crud.domain.Stock;
import com.song.crud.service.IStockService;

/**
 * 库存Service业务层处理
 *
 * @author 宋连华
 * @date 2021-04-07
 */
@Service
public class StockServiceImpl implements IStockService {
    @Autowired
    private StockMapper stockMapper;

    /**
     * 查询库存
     *
     * @param id 库存ID
     * @return 库存
     */
    @Override
    public Stock selectStockById(Long id) {
        return stockMapper.selectStockById(id);
    }

    /**
     * 查询库存列表
     *
     * @param stock 库存
     * @return 库存
     */
    @Override
    public List<Stock> selectStockList(Stock stock) {
        return stockMapper.selectStockList(stock);
    }

    /**
     * 新增库存
     *
     * @param stock 库存
     * @return 结果
     */
    @Override
    public int insertStock(Stock stock) {
        return stockMapper.insertStock(stock);
    }

    /**
     * 修改库存
     *
     * @param stock 库存
     * @return 结果
     */
    @Override
    public int updateStock(Stock stock) {
        return stockMapper.updateStock(stock);
    }

    @Override

    public int updateDrugName(String old, String xin) {
        return stockMapper.updateDrugName(old, xin);
    }

    @Override
    public int updateStatus1( )

    {
        return stockMapper.updateStatus1();
    }
    @Override
    public int updateStatus0( )

    {
        return stockMapper.updateStatus0();
    }

    /**
     * 批量删除库存
     *
     * @param ids 需要删除的库存ID
     * @return 结果
     */
    @Override
    public int deleteStockByIds(Long[] ids) {
        return stockMapper.deleteStockByIds(ids);
    }

    /**
     * 删除库存信息
     *
     * @param id 库存ID
     * @return 结果
     */
    @Override
    public int deleteStockById(Long id) {
        return stockMapper.deleteStockById(id);
    }
}
