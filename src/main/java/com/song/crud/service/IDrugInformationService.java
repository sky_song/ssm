package com.song.crud.service;

import java.util.List;
import com.song.crud.domain.DrugInformation;

/**
 * 药品信息Service接口
 * 
 * @author 宋连华
 * @date 2021-04-07
 */
public interface IDrugInformationService 
{
    /**
     * 查询药品信息
     * 
     * @param id 药品信息ID
     * @return 药品信息
     */
    public DrugInformation selectDrugInformationById(Long id);

    /**
     * 查询药品信息列表
     * 
     * @param drugInformation 药品信息
     * @return 药品信息集合
     */
    public List<DrugInformation> selectDrugInformationList(DrugInformation drugInformation);

    /**
     * 新增药品信息
     * 
     * @param drugInformation 药品信息
     * @return 结果
     */
    public int insertDrugInformation(DrugInformation drugInformation);

    /**
     * 修改药品信息
     * 
     * @param drugInformation 药品信息
     * @return 结果
     */
    public int updateDrugInformation(DrugInformation drugInformation);

    /**
     * 批量删除药品信息
     * 
     * @param ids 需要删除的药品信息ID
     * @return 结果
     */
    public int deleteDrugInformationByIds(Long[] ids);

    /**
     * 删除药品信息信息
     * 
     * @param id 药品信息ID
     * @return 结果
     */
    public int deleteDrugInformationById(Long id);
}
