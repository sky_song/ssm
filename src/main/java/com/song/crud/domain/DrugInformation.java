package com.song.crud.domain;


import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * 药品信息对象 drug_information
 *
 * @author 宋连华
 * @date 2021-04-07
 */
public class DrugInformation extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private Long id;

    /** 药名 */
    private String drugName;

    /** 代表多少个基本单位为一盒 */
    private String specifications;

    /** 基本单位 */
    private String basicUnit;

    /** 最低库存要求 */
    private Long minimumStock;

    /** 停用标志 */
    private Long status;

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId()
    {
        return id;
    }
    public void setDrugName(String drugName)
    {
        this.drugName = drugName;
    }

    public String getDrugName()
    {
        return drugName;
    }
    public void setSpecifications(String  specifications)
    {
        this.specifications = specifications;
    }

    public String  getSpecifications()
    {
        return specifications;
    }
    public void setBasicUnit(String basicUnit)
    {
        this.basicUnit = basicUnit;
    }

    public String getBasicUnit()
    {
        return basicUnit;
    }
    public void setMinimumStock(Long minimumStock)
    {
        this.minimumStock = minimumStock;
    }

    public Long getMinimumStock()
    {
        return minimumStock;
    }
    public void setStatus(Long status)
    {
        this.status = status;
    }

    public Long getStatus()
    {
        return status;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("drugName", getDrugName())
            .append("specifications", getSpecifications())
            .append("basicUnit", getBasicUnit())
            .append("minimumStock", getMinimumStock())
            .append("status", getStatus())
            .toString();
    }
}
