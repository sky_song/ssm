package com.song.crud.domain;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.util.Date;

/**
 * @author 宋LS
 * @version 1.0
 * @date 2021/3/31 19:35
 */
public class updateInfoVo {

    /** 名称 */
    private String name;

    /** 工号 */
    private String empSn;

    /** 密码 */
    private String password;

    /** 停用标志   0 没有停用   1停用*/
    private Integer status;

    /** 权限 */
    private Long permission;


    /** 性别 */
    private String sex;

    /** 出生日期 */

    private String birthday;

    /** 手机号 */
    private String phone;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmpSn() {
        return empSn;
    }

    public void setEmpSn(String empSn) {
        this.empSn = empSn;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Long getPermission() {
        return permission;
    }

    public void setPermission(Long permission) {
        this.permission = permission;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }
}
