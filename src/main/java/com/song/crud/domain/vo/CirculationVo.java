package com.song.crud.domain.vo;

import com.song.crud.domain.CirculationType;
import com.song.crud.domain.Warehouse;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * @author 宋LS
 * @version 1.0
 * @date 2021/4/8 22:21
 */
public class CirculationVo {
    private static final long serialVersionUID = 1L;

    /** 流通记录表 */
    private Long id;

    /** 操作人默认是校验成功的工号 */
    private String name;

    /** 药品名 */
    private String drugName;

    /** 数量 */
    private Long number;

    /** 流通类型 */
    private CirculationType type;

    /** 指定仓库 */
    private Warehouse warehouse;



}
