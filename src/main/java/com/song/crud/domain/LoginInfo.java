package com.song.crud.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;


/**
 * 登录信息对象 login_info
 *
 * @author song
 * @date 2021-03-24
 */
public class LoginInfo extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** id */
    private String id;

    /** 名称 */
    private String name;

    /** 工号 */
    private String empSn;

    /** 密码 */
    private String password;

    /** 停用标志   0 没有停用   1停用*/
    private Integer status;

    /** 权限 */
    private Long permission;

    public void setId(String id)
    {
        this.id = id;
    }

    public String getId()
    {
        return id;
    }
    public void setName(String name)
    {
        this.name = name;
    }

    public String getName()
    {
        return name;
    }
    public void setEmpSn(String empSn)
    {
        this.empSn = empSn;
    }

    public String getEmpSn()
    {
        return empSn;
    }
    public void setPassword(String password)
    {
        this.password = password;
    }

    public String getPassword()
    {
        return password;
    }
    public void setStatus(Integer status)
    {
        this.status = status;
    }

    public Integer getStatus()
    {
        return status;
    }
    public void setPermission(Long permission)
    {
        this.permission = permission;
    }

    public Long getPermission()
    {
        return permission;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("name", getName())
            .append("empSn", getEmpSn())
            .append("password", getPassword())
            .append("status", getStatus())
            .append("permission", getPermission())
            .toString();
    }
}
