package com.song.crud.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * 库存对象 stock
 *
 * @author 宋连华
 * @date 2021-04-07
 */
public class Stock extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private Long id;

    /** 药名 */
    private String drugName;

    /** 剩余库存 */
    private Long surplusStock;

    /** 仓库id */
    private Long warehouseId;

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId()
    {
        return id;
    }
    public void setDrugName(String drugName)
    {
        this.drugName = drugName;
    }

    public String getDrugName()
    {
        return drugName;
    }
    public void setSurplusStock(Long surplusStock)
    {
        this.surplusStock = surplusStock;
    }

    public Long getSurplusStock()
    {
        return surplusStock;
    }
    public void setWarehouseId(Long warehouseId)
    {
        this.warehouseId = warehouseId;
    }

    public Long getWarehouseId()
    {
        return warehouseId;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("drugName", getDrugName())
            .append("surplusStock", getSurplusStock())
            .append("warehouseId", getWarehouseId())
            .toString();
    }
}
