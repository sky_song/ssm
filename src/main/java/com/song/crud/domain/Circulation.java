package com.song.crud.domain;


import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * 流通记录对象 circulation
 *
 * @author 宋连华
 * @date 2021-04-07
 */
public class Circulation extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 流通记录表 */
    private Long id;

    /** 操作人默认是校验成功的工号 */
    private String name;

    /** 药品名 */
    private String drugName;

    /** 数量 */
    private Long number;

    /** 流通类型 */
    private Long typeId;

    /** 指定仓库 */
    private Long warehouseId;

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId()
    {
        return id;
    }
    public void setName(String name)
    {
        this.name = name;
    }

    public String getName()
    {
        return name;
    }
    public void setDrugName(String drugName)
    {
        this.drugName = drugName;
    }

    public String getDrugName()
    {
        return drugName;
    }
    public void setNumber(Long number)
    {
        this.number = number;
    }

    public Long getNumber()
    {
        return number;
    }
    public void setTypeId(Long typeId)
    {
        this.typeId = typeId;
    }

    public Long getTypeId()
    {
        return typeId;
    }
    public void setWarehouseId(Long warehouseId)
    {
        this.warehouseId = warehouseId;
    }

    public Long getWarehouseId()
    {
        return warehouseId;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("name", getName())
            .append("drugName", getDrugName())
            .append("number", getNumber())
            .append("typeId", getTypeId())
            .append("warehouseId", getWarehouseId())
            .append("createTime", getCreateTime())
            .toString();
    }
}
