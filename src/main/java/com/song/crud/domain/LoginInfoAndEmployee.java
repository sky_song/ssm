package com.song.crud.domain;

/**
 * @author 宋LS
 * @version 1.0
 * @date 2021/3/30 20:08
 */
public class LoginInfoAndEmployee {

    private  LoginInfo loginInfo;
    private  Employee employee;

    public LoginInfo getLoginInfo() {
        return loginInfo;
    }

    public void setLoginInfo(LoginInfo loginInfo) {
        this.loginInfo = loginInfo;
    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }
}
