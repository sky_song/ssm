package com.song.crud.utils;

import com.song.crud.domain.AjaxResult;
import com.song.crud.domain.LoginInfo;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * @author 宋LS
 * @version 1.0
 * @date 2021/4/8 19:54
 */
public class GetUserInfo {

    public  static LoginInfo getAttribute(HttpServletRequest request, HttpSession session){
        Cookie[] cookies = request.getCookies();
        if (cookies != null) {
            for (Cookie cookie : cookies) {
                if (cookie.getName().equals("userinfo")) {
                    LoginInfo attribute = (LoginInfo) session.getAttribute(cookie.getValue());

                    return attribute;
                }
            }
        }
        return  null;
    }
}
