package com.song.crud.utils;

import com.song.crud.service.IStockService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * @author 宋LS
 * @version 1.0
 * @date 2021/4/9 17:30
 */

@Component
public class Task {
    @Autowired
    private IStockService stockService;

    @Scheduled(cron = "0 */1 * * * ?")
    public void UpdateStopStatus() {

        stockService.updateStatus1();
        stockService.updateStatus0();

    }
}
