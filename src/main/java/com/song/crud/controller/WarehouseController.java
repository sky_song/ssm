package com.song.crud.controller;

import java.util.List;

import com.song.crud.common.BaseController;
import com.song.crud.common.page.TableDataInfo;
import com.song.crud.domain.AjaxResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


import com.song.crud.domain.Warehouse;
import com.song.crud.service.IWarehouseService;


/**
 * 仓库Controller
 *
 * @author 宋连华
 * @date 2021-04-07
 */
@RestController
@RequestMapping("/crud/warehouse")
public class WarehouseController extends BaseController
{
    @Autowired
    private IWarehouseService warehouseService;

    /**
     * 查询仓库列表
     */
    @GetMapping("/list")
    public TableDataInfo list(Warehouse warehouse)
    {
        startPage();
        List<Warehouse> list = warehouseService.selectWarehouseList(warehouse);
        return getDataTable(list);
    }

    /**
     * 导出仓库列表
     */


    /**
     * 获取仓库详细信息
     */
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(warehouseService.selectWarehouseById(id));
    }

    /**
     * 新增仓库
     */
    @PostMapping
    public AjaxResult add(@RequestBody Warehouse warehouse)
    {
        return AjaxResult.success(warehouseService.insertWarehouse(warehouse));
    }

    /**
     * 修改仓库
     */
    @PutMapping
    public AjaxResult edit(@RequestBody Warehouse warehouse)
    {
        return toAjax(warehouseService.updateWarehouse(warehouse));
    }

    /**
     * 删除仓库
     */
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(warehouseService.deleteWarehouseByIds(ids));
    }
}
