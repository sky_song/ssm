package com.song.crud.controller;

import java.util.List;
import java.util.UUID;

import com.song.crud.common.BaseController;
import com.song.crud.common.StringUtils;
import com.song.crud.common.page.TableDataInfo;
import com.song.crud.domain.AjaxResult;
import com.song.crud.domain.Employee;
import com.song.crud.domain.updateInfoVo;
import com.song.crud.service.IEmployeeService;
import com.song.crud.utils.Md5Utils;
import org.aspectj.weaver.loadtime.Aj;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import com.song.crud.domain.LoginInfo;
import com.song.crud.service.ILoginInfoService;
import sun.security.provider.MD5;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


/**
 * 登录信息Controller
 *
 * @author song
 * @date 2021-03-24
 */
@RestController
@RequestMapping("/crud/info")
public class LoginInfoController extends BaseController {
    @Autowired
    private ILoginInfoService loginInfoService;
    @Autowired
    private IEmployeeService employeeService;

    /**
     * 查询登录信息列表
     */
    @RequestMapping("login")
    public AjaxResult login(@RequestBody LoginInfo loginInfo, HttpSession session, HttpServletResponse response) {
        String hash = Md5Utils.hash(loginInfo.getPassword());
        loginInfo.setPassword("");
        List<LoginInfo> loginInfos = loginInfoService.selectLoginInfoList(loginInfo);
        if (loginInfos != null && loginInfos.size() > 0) {
            if (loginInfos.get(0).getStatus() == 0) {
                if (hash.equals(loginInfos.get(0).getPassword())) {
                    loginInfos.get(0).setPassword("你不可能知道密码");
                    session.setAttribute(loginInfo.getEmpSn(), loginInfos.get(0));
                    Cookie cookie = new Cookie("userinfo", loginInfo.getEmpSn());
                    cookie.setPath("/");
                    response.addCookie(cookie);
                    return AjaxResult.success(loginInfos.get(0));
                } else {
                    return AjaxResult.success("密码错误");
                }
            } else {
                return AjaxResult.error("账号已经停用");
            }
        }
        return AjaxResult.error("工号不存在");

    }

    @RequestMapping("getlogininfo")
    public AjaxResult getLoginInfo(HttpServletRequest request, HttpSession session, HttpServletResponse response) {
        Cookie[] cookies = request.getCookies();
        if (cookies != null) {
            for (Cookie cookie : cookies) {
                if (cookie.getName().equals("userinfo")) {
                    Object attribute = session.getAttribute(cookie.getValue());
                    if (attribute != null) {
                        return AjaxResult.success(attribute);
                    }
                    cookie.setValue("");
                    response.addCookie(cookie);
                }
            }
        }
        return AjaxResult.error("用户没有登录");

    }


    @RequestMapping("/listlogin")
    public TableDataInfo list(LoginInfo loginInfo) {
        startPage();
        List<LoginInfo> list = loginInfoService.selectLoginInfoList(loginInfo);
        return getDataTable(list);
    }

    @RequestMapping("/logininfoandemploy")
    public TableDataInfo LoginInfoAndemployResult(String empSn) {
        startPage();
        LoginInfo loginInfo = new LoginInfo();
        loginInfo.setEmpSn(empSn);
        List<LoginInfo> list = loginInfoService.LoginInfoAndemployResult(loginInfo);
        return getDataTable(list);
    }

    /**
     * 导出登录信息列表
     */


    /**
     * 获取登录信息详细信息
     */
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") String id) {
        return AjaxResult.success(loginInfoService.selectLoginInfoById(id));
    }

    /**
     * 新增登录信息
     */

    @PostMapping("addinfo")
    public AjaxResult add(@RequestBody updateInfoVo vo) {

        if (!StringUtils.isEmpty(vo.getPassword())) {
            vo.setPassword(Md5Utils.hash(vo.getPassword()));
        }
        LoginInfo loginInfo = new LoginInfo();
        Employee employee = new Employee();
        BeanUtils.copyProperties(vo, loginInfo);
        BeanUtils.copyProperties(vo, employee);
        loginInfo.setId(UUID.randomUUID().toString().replace("-", ""));
        employee.setId(UUID.randomUUID().toString().replace("-", ""));
        loginInfoService.insertLoginInfo(loginInfo);
        employeeService.insertEmployee(employee);
        return success();
    }

    /**
     * 修改登录信息
     */


    /**
     * 删除登录信息
     */

//    @DeleteMapping("/{ids}")
//    public AjaxResult remove(@PathVariable String[] ids) {
//        System.out.println("ids = " + ids);
//        return toAjax(loginInfoService.deleteLoginInfoByIds(ids));
//    }
    @DeleteMapping("/{id}")
    public AjaxResult remove(@PathVariable String id) {
        employeeService.deleteEmployeeById(id);
        loginInfoService.deleteLoginInfoById(id);
        return success();
    }

    @PostMapping("/updateinfo")
    public AjaxResult UpdateInfo(@RequestBody updateInfoVo vo) {
        if (!StringUtils.isEmpty(vo.getPassword())) {
            vo.setPassword(Md5Utils.hash(vo.getPassword()));
        }
        LoginInfo loginInfo = new LoginInfo();
        Employee employee = new Employee();
        BeanUtils.copyProperties(vo, loginInfo);
        BeanUtils.copyProperties(vo, employee);
//修改
        loginInfoService.updateLoginInfo(loginInfo);
        employeeService.updateEmployee(employee);
        return AjaxResult.success();
    }


}
