package com.song.crud.controller;

import java.util.List;

import com.song.crud.common.BaseController;
import com.song.crud.common.page.TableDataInfo;
import com.song.crud.domain.AjaxResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.song.crud.domain.Unit;
import com.song.crud.service.IUnitService;


/**
 * 单位Controller
 *
 * @author 宋连华
 * @date 2021-04-07
 */
@RestController
@RequestMapping("/crud/unit")
public class UnitController extends BaseController
{
    @Autowired
    private IUnitService unitService;

    /**
     * 查询单位列表
     */
    @GetMapping("/list")
    public TableDataInfo list(Unit unit)
    {
        startPage();
        List<Unit> list = unitService.selectUnitList(unit);
        return getDataTable(list);
    }

    /**
     * 导出单位列表
     */


    /**
     * 获取单位详细信息
     */
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(unitService.selectUnitById(id));
    }

    /**
     * 新增单位
     */
    @PostMapping
    public AjaxResult add(@RequestBody Unit unit)
    {
        return AjaxResult.success(unitService.insertUnit(unit));
    }

    /**
     * 修改单位
     */
    @PutMapping
    public AjaxResult edit(@RequestBody Unit unit)
    {
        return AjaxResult.success(unitService.updateUnit(unit));
    }

    /**
     * 删除单位
     */
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(unitService.deleteUnitByIds(ids));
    }
}
