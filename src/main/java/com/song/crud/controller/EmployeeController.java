package com.song.crud.controller;

import java.util.List;

import com.song.crud.common.BaseController;
import com.song.crud.common.page.TableDataInfo;
import com.song.crud.domain.AjaxResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import com.song.crud.domain.Employee;
import com.song.crud.service.IEmployeeService;


/**
 * 员工信息Controller
 *
 * @author song
 * @date 2021-03-24
 */
@RestController
@RequestMapping("/crud/employee")
public class EmployeeController extends BaseController {
    @Autowired
    private IEmployeeService employeeService;

    /**
     * 查询员工信息列表
     */
    @RequestMapping("/list")

    public TableDataInfo list(Employee employee)
    {
        startPage();
        List<Employee> list = employeeService.selectEmployeeList(employee);
        return getDataTable(list);
    }

    /**
     * 导出员工信息列表
     */



    /**
     * 获取员工信息详细信息
     */
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") String id)
    {
        return AjaxResult.success(employeeService.selectEmployeeById(id));
    }

    /**
     * 新增员工信息
     */

    @PostMapping
    public AjaxResult add(@RequestBody Employee employee)
    {
        employeeService.insertEmployee(employee);

        return AjaxResult.success();
    }

    /**
     * 修改员工信息
     */

    @PutMapping
    public AjaxResult edit(@RequestBody Employee employee)
    {
        return toAjax(employeeService.updateEmployee(employee));
    }

    /**
     * 删除员工信息
     */

	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable String[] ids)
    {
        return toAjax(employeeService.deleteEmployeeByIds(ids));
    }
}
