package com.song.crud.controller;

import java.util.List;

import com.song.crud.common.BaseController;
import com.song.crud.common.page.TableDataInfo;
import com.song.crud.domain.AjaxResult;
import com.song.crud.domain.LoginInfo;
import com.song.crud.utils.GetUserInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.song.crud.domain.Stock;
import com.song.crud.service.IStockService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;


/**
 * 库存Controller
 *
 * @author 宋连华
 * @date 2021-04-07
 */
@RestController
@RequestMapping("/crud/stock")
public class StockController extends BaseController {
    @Autowired
    private IStockService stockService;

    /**
     * 查询库存列表
     */
    @GetMapping("/list")
    public TableDataInfo list(Stock stock) {
        startPage();
        List<Stock> list = stockService.selectStockList(stock);
        return getDataTable(list);
    }

    /**
     * 导出库存列表
     */


    /**
     * 获取库存详细信息
     */
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return AjaxResult.success(stockService.selectStockById(id));
    }

    /**
     * 新增库存
     */
    @PostMapping
    public AjaxResult add(@RequestBody Stock stock) {
        stockService.insertStock(stock);
        return success();
    }

    /**
     * 修改库存
     */
    @PutMapping
    public AjaxResult edit(@RequestBody Stock stock, HttpServletRequest request, HttpSession session) {
        LoginInfo attribute = GetUserInfo.getAttribute(request, session);
        Long permission = attribute.getPermission();
        if (permission == 1) {
            return AjaxResult.error("操作权限不足");
        }
        return AjaxResult.success(stockService.updateStock(stock));
    }

    /**
     * 删除库存
     */
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids) {
        return AjaxResult.success(stockService.deleteStockByIds(ids));
    }



}
