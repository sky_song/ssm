package com.song.crud.controller;

import java.util.List;

import com.song.crud.common.BaseController;
import com.song.crud.common.page.TableDataInfo;
import com.song.crud.domain.*;
import com.song.crud.service.ICirculationTypeService;
import com.song.crud.service.IDrugInformationService;
import com.song.crud.service.IStockService;
import com.song.crud.utils.GetUserInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.song.crud.service.ICirculationService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * 流通记录Controller
 *
 * @author 宋连华
 * @date 2021-04-07
 */
@RestController
@RequestMapping("/crud/circulation")
public class CirculationController extends BaseController {
    @Autowired
    private ICirculationService circulationService;
    @Autowired
    private IDrugInformationService informationService;

    @Autowired
    private ICirculationTypeService iCirculationTypeService;
    @Autowired
    private IStockService iStockService;


    /**
     * 查询流通记录列表
     */
    @GetMapping("/list")
    public TableDataInfo list(Circulation circulation) {
        startPage();
        List<Circulation> list = circulationService.selectCirculationList(circulation);
        return getDataTable(list);
    }


    /**
     * 获取流通记录详细信息
     */
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return AjaxResult.success(circulationService.selectCirculationById(id));
    }
    /**
     * 新增流通记录
     */
    @PostMapping
    public AjaxResult add(@RequestBody Circulation circulation, HttpServletRequest request, HttpSession session) {
        LoginInfo attribute = GetUserInfo.getAttribute(request, session);
        if (attribute == null) {
            return error("没有登录");
        }
//        药品信息是否存在
        DrugInformation druginformation = new DrugInformation();
        druginformation.setDrugName(circulation.getDrugName());
        List<DrugInformation> drugInformations = informationService.selectDrugInformationList(druginformation);
        if (drugInformations != null && drugInformations.size() > 0) {
            druginformation = drugInformations.get(0);
//            重新设置数量
            if (druginformation.getStatus() == 1) {
                return error("药品已停用");
            }

            String specifications = druginformation.getSpecifications();
            long l = Long.parseLong(specifications);
            circulation.setNumber(circulation.getNumber() * l);
        } else {
            return error("没有该药品");
        }
//  查询  药品名和仓库  如果有  在判断是入库还是出库
//    数据库已经存在库存
        Stock stock = new Stock();
        stock.setDrugName(circulation.getDrugName());
        stock.setWarehouseId(circulation.getWarehouseId());
        List<Stock> stocks = iStockService.selectStockList(stock);
        if (stocks == null || stocks.size() == 0) {
            return error("仓库没有此类药品");
        }
//        有库存
        CirculationType circulationType = new CirculationType();
        circulationType.setType("退药");
        List<CirculationType> circulationTypes = iCirculationTypeService.selectCirculationTypeList(circulationType);
        circulationType.setType("入库");
        List<CirculationType> circulationTypes1 = iCirculationTypeService.selectCirculationTypeList(circulationType);
        Stock stock1 = stocks.get(0);
        if (circulation.getTypeId() == circulationTypes.get(0).getId() || circulation.getTypeId() == circulationTypes1.get(0).getId()) {
//  判断入库   退药
//                修改库存
//            原有库存
            Long surplusStock = stock1.getSurplusStock();
            Long number = circulation.getNumber();
            circulation.setName(attribute.getEmpSn());
            circulationService.insertCirculation(circulation);
            stock1.setSurplusStock(surplusStock + number);
            iStockService.updateStock(stock1);
        } else {
//                出库 发药
//                修改  库存确认出库 发药
            if (stock1.getSurplusStock() > 0) {
                circulation.setName(attribute.getEmpSn());
                circulationService.insertCirculation(circulation);
                Long surplusStock = stock1.getSurplusStock();
                Long number = circulation.getNumber();
                long surplus = surplusStock - number;
                if (surplus < 0) {
                    return error("库存数量不够");
                }
                stock1.setSurplusStock(surplus);
                iStockService.updateStock(stock1);
            } else {
                return error("没有库存");
            }


        }


        return success();
    }

    /**
     * 修改流通记录
     */
    @PutMapping
    public AjaxResult edit(@RequestBody Circulation circulation) {
        return toAjax(circulationService.updateCirculation(circulation));
    }

    /**
     * 删除流通记录
     */
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids) {
        return toAjax(circulationService.deleteCirculationByIds(ids));
    }
}
