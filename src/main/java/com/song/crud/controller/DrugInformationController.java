package com.song.crud.controller;

import java.util.List;

import com.song.crud.common.BaseController;
import com.song.crud.common.page.TableDataInfo;

import com.song.crud.domain.AjaxResult;
import com.song.crud.domain.LoginInfo;
import com.song.crud.domain.Stock;
import com.song.crud.service.IStockService;
import com.song.crud.utils.GetUserInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.song.crud.domain.DrugInformation;
import com.song.crud.service.IDrugInformationService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;


/**
 * 药品信息Controller
 *
 * @author 宋连华
 * @date 2021-04-07
 */
@RestController
@RequestMapping("/crud/information")
public class DrugInformationController extends BaseController {
    @Autowired
    private IDrugInformationService drugInformationService;

    @Autowired
    private IStockService stockService;

    /**
     * 查询药品信息列表
     */
    @GetMapping("/list")
    public TableDataInfo list(DrugInformation drugInformation) {
        startPage();
        List<DrugInformation> list = drugInformationService.selectDrugInformationList(drugInformation);
        return getDataTable(list);
    }

    /**
     * 导出药品信息列表
     */

    /**
     * 获取药品信息详细信息
     */
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return AjaxResult.success(drugInformationService.selectDrugInformationById(id));
    }

    /**
     * 新增药品信息
     */
    @PostMapping
    public AjaxResult add(@RequestBody DrugInformation drugInformation, HttpServletRequest request, HttpSession session) {

        LoginInfo attribute = GetUserInfo.getAttribute(request, session);
        if (attribute.getPermission() == 1) {
            return AjaxResult.error("权限不足");
        }
        DrugInformation drugInformationdb = new DrugInformation();
        drugInformationdb.setDrugName(drugInformation.getDrugName());
        List<DrugInformation> drugInformations = drugInformationService.selectDrugInformationList(drugInformationdb);
        if (drugInformations != null && drugInformations.size() > 0) {
            return AjaxResult.error("已经存在此药品");
        }
        drugInformationService.insertDrugInformation(drugInformation);

        return AjaxResult.success();
    }

    /**
     * 修改药品信息
     */
    @PutMapping
    public AjaxResult edit(@RequestBody DrugInformation drugInformation, HttpServletRequest request, HttpSession session) {

        LoginInfo attribute = GetUserInfo.getAttribute(request, session);
        if (attribute.getPermission() == 1) {
            return AjaxResult.error("权限不足");
        }
//        如果修改了药名
        DrugInformation drugInformationold = drugInformationService.selectDrugInformationById(drugInformation.getId());
        System.out.println("drugInformationold = " + drugInformationold);
        System.out.println("drugInformation = " + drugInformation);
        if (!drugInformationold.getDrugName().equals(drugInformation.getDrugName())) {

            stockService.updateDrugName(drugInformationold.getDrugName(), drugInformation.getDrugName());
        }
        drugInformationService.updateDrugInformation(drugInformation);
        return AjaxResult.success();
    }

    /**
     * 删除药品信息
     */
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids) {
        return toAjax(drugInformationService.deleteDrugInformationByIds(ids));
    }
}
