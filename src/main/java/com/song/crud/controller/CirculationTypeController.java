package com.song.crud.controller;

import java.util.List;

import com.song.crud.common.BaseController;
import com.song.crud.common.page.TableDataInfo;
import com.song.crud.domain.AjaxResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.song.crud.domain.CirculationType;
import com.song.crud.service.ICirculationTypeService;


/**
 * 流通记录Controller
 *
 * @author 宋连华
 * @date 2021-04-07
 */
@RestController
@RequestMapping("/crud/type")
public class CirculationTypeController extends BaseController
{
    @Autowired
    private ICirculationTypeService circulationTypeService;

    /**
     * 查询流通记录列表
     */
    @GetMapping("/list")
    public TableDataInfo list(CirculationType circulationType)
    {
        startPage();
        List<CirculationType> list = circulationTypeService.selectCirculationTypeList(circulationType);
        return getDataTable(list);
    }

    /**
     * 导出流通记录列表
     */


    /**
     * 获取流通记录详细信息
     */
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(circulationTypeService.selectCirculationTypeById(id));
    }

    /**
     * 新增流通记录
     */
    @PostMapping
    public AjaxResult add(@RequestBody CirculationType circulationType)
    {
        return AjaxResult.success(circulationTypeService.insertCirculationType(circulationType));
    }

    /**
     * 修改流通记录
     */
    @PutMapping
    public AjaxResult edit(@RequestBody CirculationType circulationType)
    {
        return toAjax(circulationTypeService.updateCirculationType(circulationType));
    }

    /**
     * 删除流通记录
     */
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(circulationTypeService.deleteCirculationTypeByIds(ids));
    }
}
