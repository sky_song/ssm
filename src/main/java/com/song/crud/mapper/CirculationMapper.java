package com.song.crud.mapper;

import java.util.List;
import com.song.crud.domain.Circulation;

/**
 * 流通记录Mapper接口
 * 
 * @author 宋连华
 * @date 2021-04-07
 */
public interface CirculationMapper 
{
    /**
     * 查询流通记录
     * 
     * @param id 流通记录ID
     * @return 流通记录
     */
    public Circulation selectCirculationById(Long id);

    /**
     * 查询流通记录列表
     * 
     * @param circulation 流通记录
     * @return 流通记录集合
     */
    public List<Circulation> selectCirculationList(Circulation circulation);

    /**
     * 新增流通记录
     * 
     * @param circulation 流通记录
     * @return 结果
     */
    public int insertCirculation(Circulation circulation);

    /**
     * 修改流通记录
     * 
     * @param circulation 流通记录
     * @return 结果
     */
    public int updateCirculation(Circulation circulation);

    /**
     * 删除流通记录
     * 
     * @param id 流通记录ID
     * @return 结果
     */
    public int deleteCirculationById(Long id);

    /**
     * 批量删除流通记录
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteCirculationByIds(Long[] ids);
}
