package com.song.crud.mapper;

import java.util.List;
import com.song.crud.domain.Unit;

/**
 * 单位Mapper接口
 * 
 * @author 宋连华
 * @date 2021-04-07
 */
public interface UnitMapper 
{
    /**
     * 查询单位
     * 
     * @param id 单位ID
     * @return 单位
     */
    public Unit selectUnitById(Long id);

    /**
     * 查询单位列表
     * 
     * @param unit 单位
     * @return 单位集合
     */
    public List<Unit> selectUnitList(Unit unit);

    /**
     * 新增单位
     * 
     * @param unit 单位
     * @return 结果
     */
    public int insertUnit(Unit unit);

    /**
     * 修改单位
     * 
     * @param unit 单位
     * @return 结果
     */
    public int updateUnit(Unit unit);

    /**
     * 删除单位
     * 
     * @param id 单位ID
     * @return 结果
     */
    public int deleteUnitById(Long id);

    /**
     * 批量删除单位
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteUnitByIds(Long[] ids);
}
