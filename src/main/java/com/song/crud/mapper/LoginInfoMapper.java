package com.song.crud.mapper;

import java.util.List;
import com.song.crud.domain.LoginInfo;

/**
 * 登录信息Mapper接口
 *
 * @author song
 * @date 2021-03-24
 */
public interface LoginInfoMapper
{
    /**
     * 查询登录信息
     *
     * @param id 登录信息ID
     * @return 登录信息
     */
    public LoginInfo selectLoginInfoById(String id);

    /**
     * 查询登录信息列表
     *
     * @param loginInfo 登录信息
     * @return 登录信息集合
     */
    public List<LoginInfo> selectLoginInfoList(LoginInfo loginInfo);
    public List<LoginInfo> LoginInfoAndemployResult(LoginInfo loginInfo);

    /**
     * 新增登录信息
     *
     * @param loginInfo 登录信息
     * @return 结果
     */
    public int insertLoginInfo(LoginInfo loginInfo);

    /**
     * 修改登录信息
     *
     * @param loginInfo 登录信息
     * @return 结果
     */
    public int updateLoginInfo(LoginInfo loginInfo);

    /**
     * 删除登录信息
     *
     * @param id 登录信息ID
     * @return 结果
     */
    public int deleteLoginInfoById(String id);

    /**
     * 批量删除登录信息
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteLoginInfoByIds(String[] ids);
}
