package com.song.crud.mapper;

import java.util.List;
import com.song.crud.domain.CirculationType;

/**
 * 流通记录Mapper接口
 * 
 * @author 宋连华
 * @date 2021-04-07
 */
public interface CirculationTypeMapper 
{
    /**
     * 查询流通记录
     * 
     * @param id 流通记录ID
     * @return 流通记录
     */
    public CirculationType selectCirculationTypeById(Long id);

    /**
     * 查询流通记录列表
     * 
     * @param circulationType 流通记录
     * @return 流通记录集合
     */
    public List<CirculationType> selectCirculationTypeList(CirculationType circulationType);

    /**
     * 新增流通记录
     * 
     * @param circulationType 流通记录
     * @return 结果
     */
    public int insertCirculationType(CirculationType circulationType);

    /**
     * 修改流通记录
     * 
     * @param circulationType 流通记录
     * @return 结果
     */
    public int updateCirculationType(CirculationType circulationType);

    /**
     * 删除流通记录
     * 
     * @param id 流通记录ID
     * @return 结果
     */
    public int deleteCirculationTypeById(Long id);

    /**
     * 批量删除流通记录
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteCirculationTypeByIds(Long[] ids);
}
