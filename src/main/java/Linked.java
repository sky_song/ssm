
public class Linked {
    Node head = null;
    int count = 0;

    public void addFirst(Node data) {
        data.next = head;
        head = data;
        count++;
    }

    public void addLast(Node data) {
        if (head != null) {
            Node temp = head;
            while (temp.next != null) {
                temp = temp.next;
            }
            temp.next = data;
        } else {
            head = data;
        }
    }

    public void scan() {
        Node curNode = head;
        while (curNode != null) {
            System.out.print(curNode.data + " ");
            curNode = curNode.next;
        }
        System.out.println();
    }
}
