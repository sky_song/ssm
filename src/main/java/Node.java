/**
 * @author 宋LS
 * @version 1.0
 * @date 2021/3/25 9:18
 */
public class Node {
    Node next =null;
    Object data;

    public Node(Object data) {
        this.data = data;
    }

    public Node getNext() {
        return next;
    }

    public void setNext(Node next) {
        this.next = next;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }
}
