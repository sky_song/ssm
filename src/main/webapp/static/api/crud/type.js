import request from '@/utils/request'

// 查询流通记录列表
export function listType(query) {
  return request({
    url: '/crud/type/list',
    method: 'get',
    params: query
  })
}

// 查询流通记录详细
export function getType(id) {
  return request({
    url: '/crud/type/' + id,
    method: 'get'
  })
}

// 新增流通记录
export function addType(data) {
  return request({
    url: '/crud/type',
    method: 'post',
    data: data
  })
}

// 修改流通记录
export function updateType(data) {
  return request({
    url: '/crud/type',
    method: 'put',
    data: data
  })
}

// 删除流通记录
export function delType(id) {
  return request({
    url: '/crud/type/' + id,
    method: 'delete'
  })
}

// 导出流通记录
export function exportType(query) {
  return request({
    url: '/crud/type/export',
    method: 'get',
    params: query
  })
}