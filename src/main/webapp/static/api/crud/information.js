import request from '@/utils/request'

// 查询药品信息列表
export function listInformation(query) {
  return axios({
    url: '/crud/information/list',
    method: 'get',
    params: query
  })
}

// 查询药品信息详细
export function getInformation(id) {
  return request({
    url: '/crud/information/' + id,
    method: 'get'
  })
}

// 新增药品信息
export function addInformation(data) {
  return request({
    url: '/crud/information',
    method: 'post',
    data: data
  })
}

// 修改药品信息
export function updateInformation(data) {
  return request({
    url: '/crud/information',
    method: 'put',
    data: data
  })
}

// 删除药品信息
export function delInformation(id) {
  return request({
    url: '/crud/information/' + id,
    method: 'delete'
  })
}

// 导出药品信息
export function exportInformation(query) {
  return request({
    url: '/crud/information/export',
    method: 'get',
    params: query
  })
}
