
// 查询流通记录列表
export function listCirculation(query) {
  return axios({
    url: '/crud/circulation/list',
    method: 'get',
    params: query
  })
}

// 查询流通记录详细
export function getCirculation(id) {
  return axios({
    url: '/crud/circulation/' + id,
    method: 'get'
  })
}

// 新增流通记录
export function addCirculation(data) {
  return axios({
    url: '/crud/circulation',
    method: 'post',
    data: data
  })
}

// 修改流通记录
export function updateCirculation(data) {
  return axios({
    url: '/crud/circulation',
    method: 'put',
    data: data
  })
}

// 删除流通记录
export function delCirculation(id) {
  return axios({
    url: '/crud/circulation/' + id,
    method: 'delete'
  })
}

// 导出流通记录
export function exportCirculation(query) {
  return axios({
    url: '/crud/circulation/export',
    method: 'get',
    params: query
  })
}
