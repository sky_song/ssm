import request from '@/utils/request'

// 查询库存列表
export function listStock(query) {
  return  axios({
    url: '/crud/stock/list',
    method: 'get',
    params: query
  })
}

// 查询库存详细
export function getStock(id) {
  return request({
    url: '/crud/stock/' + id,
    method: 'get'
  })
}

// 新增库存
export function addStock(data) {
  return request({
    url: '/crud/stock',
    method: 'post',
    data: data
  })
}

// 修改库存
export function updateStock(data) {
  return request({
    url: '/crud/stock',
    method: 'put',
    data: data
  })
}

// 删除库存
export function delStock(id) {
  return request({
    url: '/crud/stock/' + id,
    method: 'delete'
  })
}

// 导出库存
export function exportStock(query) {
  return request({
    url: '/crud/stock/export',
    method: 'get',
    params: query
  })
}
