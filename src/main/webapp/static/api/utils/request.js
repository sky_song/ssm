export function request(url,method,params) {

  return    axios({
        method: method,
        url: url,
        data:params
    });

}
