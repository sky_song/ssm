const loginUrl = "../crud/info/login";

$(function () {
//登录
    $("#loginBut").click(function () {
        var username = $("#loginname").val()
        var password = $("#loginpwd").val()
        var data = {
            empSn: username,
            password: password
        };
        $("#alertmsg").empty();
        $.ajax({
            type: 'POST',
            contentType: "application/json;charset=utf-8",
            url: loginUrl,
            dataType: "json",
            data: JSON.stringify(data),
            success: function (res) {

                if (res.code == 500) {
                    $("#alertmsg").append("<div class=\"alert alert-danger\" role=\"alert\" >" + res.msg + "</div>")

                }else if (res.code == 200){

                    if(res.data.permission == 1)
                    window.location="./index.html";
                    else{
                        window.location="./admin/admin.html";
                    }
                }
            },

        });


    });

//  注册
    $("#registBtn").click(function () {

    });
})

