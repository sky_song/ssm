
function ErrorMsg(test) {

    setMsg();
    $('.bs-example-modal-sm-error').modal();
    $(".modal-body").html(test)
    $('.bs-example-modal-sm-error').on('hide.bs.modal', function () {
        window.location = "/ssm/html/login.html";
    });
}

function SucceMsg(test) {
    $('.bs-example-modal-sm-succ').modal();
    $(".modal-body").html(test)
}

function setMsg() {
    $("body").remove(".bs-example-modal-sm-error");
    $("body").remove(".bs-example-modal-sm-succ");

    $("body").append(" <div class=\"modal fade bs-example-modal-sm-error\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"mySmallModalLabel\">\n" +
        "        <div class=\"modal-dialog modal-sm\" role=\"document\">\n" +
        "            <div class=\"modal-content\">\n" +
        "                <div class=\"modal-header\">\n" +
        "                    <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button>\n" +
        "                    <h4 class=\"modal-title\"><span class=\"glyphicon glyphicon-remove\" aria-hidden=\"true\"   style='color: #FF0000; font-size: 16px;margin-right: 5px;'></span>提示</h4>\n" +
        "                </div>\n" +
        "                <div class=\"modal-body\">\n" +
        "                    <p>One fine body&hellip;</p>\n" +
        "                </div>\n" +
        "            </div>\n" +
        "        </div>\n" +
        "    </div>\n" +
        "    <div class=\"modal fade bs-example-modal-sm-succ\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"mySmallModalLabel\">\n" +
        "        <div class=\"modal-dialog modal-sm\" role=\"document\">\n" +
        "            <div class=\"modal-content\">\n" +
        "                <div class=\"modal-header\">\n" +
        "                    <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button>\n" +
        "                    <h4 class=\"modal-title\"><span class=\"glyphicon glyphicon-ok\" aria-hidden=\"true\"   style='color: #00ff00; font-size: 16px;margin-right: 5px;'></span>提示</h4>\n" +
        "                </div>\n" +
        "                <div class=\"modal-body\">\n" +
        "                    <p>One fine body&hellip;</p>\n" +
        "                </div>\n" +
        "            </div>\n" +
        "        </div>\n" +
        "    </div>")
}
