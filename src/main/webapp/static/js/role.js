var pagenum = 1;
var totle = 0;
var PageSize = 20;
$(function () {


    $("#addinfo").click(function () {
        $(".modal-body").html("");
        $('.adduserinfo').modal('hide')
        var html = " <div class=\"form-group\">" +
            "     <label for=\"loginname\">名字</label>" +
            "     <input type=\"text\"  class=\"form-control\" id=\"loginname\"   placeholder=\"名字\">" +
            " </div>" +
            " <div class=\"form-group\">" +
            "     <label for=\"loginemp\">工号</label>" +
            "     <input type=\"text\"  class=\"form-control\" id=\"loginemp\"  placeholder=\"工号\">" +
            " </div>" +
            " <div class=\"form-group\">" +
            "     <label for=\"loginpassword\">密码</label>" +
            "     <input type=\"password\"  class=\"form-control\" id=\"loginpassword\" placeholder=\"密码\">" +
            " </div>" +
            " <div class=\"form-group\">" +
            "     <label for=\"birthday\">密码</label>" +
            "     <input type=\"date\"  class=\"form-control\" id=\"birthday\" placeholder=\"密码\">" +
            " </div>" +
            " <div class=\"form-group\">" +
            "     <label for=\"loginsex\" >性别</label>" +
            "     <select id=\"loginsex\" class=\"form-control\" >" +
            "         <option value=\"男\">男</option>" +
            "         <option value=\"女\">女</option>" +
            "     </select>" +
            " </div>" +
            " <div class=\"form-group\">" +
            "     <label for=\"loginphone\">手机号</label>" +
            "     <input type=\"text\"   class=\"form-control\" id=\"loginphone\" placeholder=\"手机号\">" +
            " </div>" +
            "<div class=\"form-group\">" +
            "                    <label for=\"permission\">用户角色</label>" +
            "<select id=\"permission\" class=\"form-control\">" +
            "<option value=\"0\">普通用户</option>" +
            "<option value=\"1\">管理员</option>" +
            "</select>" +
            "                </div>" +
            "<div class=\"form-group\">" +
            "                    <label for=\"permission\">是否停用</label>" +
            "<select id=\"status\" class=\"form-control\">" +
            "<option value=\"0\">可用</option>" +
            "<option value=\"1\">停用</option>" +
            "</select>" +
            "</div>";
        $("#btn_addInfo").text("新增")

        $(".modal-body").html(html)
        $('.adduserinfo').modal('show')

    })

    $("#btn_addInfo").click(function () {

        getInfoForm();

    })
    getUsers();
    $("#firstpage").click(function () {
        pagenum = 1;
        getUsers();

    })
    $("#endpage").click(function () {
        pagenum = totle / PageSize + 1;
        getUsers();
    })
    $("#previousPage").click(function () {
        if (pagenum < (totle / PageSize + 1))
            pagenum = pagenum + 1;

        getUsers();
    })
    $("#nextPage").click(function () {
        if (pagenum == 1) {
            pagenum = 1
        } else {
            pagenum = pagenum - 1;
        }
        getUsers();
    })


    $(".form_datetime").datetimepicker({
        format: 'yyyy-mm-dd hh:ii:ss',
        autoclose: true,
        language: 'zh-CN'
    });


    $("#addUserBtn").on("click", function () {
        //真正项目中，我门要做两个事情，一个前端的数据有效性校验
        //提交表单数据
        $("#addUserFrom").submit();
    });
    // $("#updateUserModal").on("hidden.bs.modal", function () {
    //     $(this).removeData("bs.modal");
    // });
    // $("#updateUserModal").on("shown.bs.modal", function () {
    //     $("#updateUserModal .selectpicker").selectpicker();
    // });
    $("#batchDelUsersBtn").on("click", function () {
        //1.获取到勾选了的复选框的dom对象，集合变量里
        var checkboxes = $(".chkone:checked");
        if (checkboxes.length == 0) {
            alert("必须勾选要删除的用户记录！")
        } else {
            //2.先获取到勾选了的复选框中的value（user的id值）
            var userIds = new Array();
            checkboxes.each(function () {
                userIds.push(this.value);
            });
            //alert(userIds);
            //3.通过ajax，向服务端提交删除用户的请求，userIds传参到服务端
            var datas = JSON.stringify(userIds);
            var flag = delSure();
            if (flag) {
                //执行删除动作
                $.ajax({
                    url: '/qihangkt/admin/batchDelUsers.html',
                    type: 'POST',
                    data: {
                        uid: datas
                    },
                    success: function (rs) {
                        //删除成功后的回调函数
//    					alert(rs);
                        if (rs == "success") {
                            $(location).attr("href", "/qihangkt/admin/userManager.html");
                        }
                    }
                });
            }
        }
    });

    $("#searchBtn").on("click", function () {
        $("#search_form").submit();
    });

});

function chkall() {
    //$(".chkall");
    $(".chkone").prop("checked", $(".chkall").prop("checked"));
}

function chkone() {
    var flag = true; //🚩
    $(".chkone").each(function (index, el) {
        var chk = $(el);
        if ($(el).prop("checked") == false) {
            flag = false;
        }
    });
    if (flag) {
        $(".chkall").prop("checked", true);
    } else {
        $(".chkall").prop("checked", false);
    }
}

function updateUserFormSubmint() {
    $("#updateUserForm").submit();
}

function delSure() {
    if (confirm("确定要删除这条记录吗？")) {
        return true;
    } else {
        return false;
    }
}

var getlogininfo = "../../crud/info/logininfoandemploy";

function getUsers() {

    $.ajax({
        type: 'get',
        url: getlogininfo + "?pageNum=" + pagenum + "&pageSize=" + PageSize,
        success: function (res) {
            if (res.code == 500) {

            } else {
                var html = "<tr>" +
                    "<td style=\"width: 30px;\"><input type=\"checkbox\" class=\"chkall\" onclick=\"chkall();\"></td>" +
                    "<td>名称</td>" +
                    "<td>工号</td>" +
                    "<td>性别</td>" +
                    "<td>手机号</td>" +
                    "<td>生日</td>" +
                    "<td>用户权限</td>" +
                    "<td style=\"width: 200px;\">操作</td>" +
                    "</tr>";
                for (let lengthKey = 0; lengthKey < res.rows.length; lengthKey++) {
                    html += "<tr>" +
                        "<td><input type=\"checkbox\" name=\"chkuser\" class=\"chkone\" onclick=\"chkone();\"></td>" +
                        "<td>" + res.rows[lengthKey].loginInfo.name + "</td>" +
                        "<td>" + res.rows[lengthKey].loginInfo.empSn + "</td>" +
                        "<td>" + res.rows[lengthKey].employee.sex + "</td>" +
                        "<td>" + res.rows[lengthKey].employee.phone + "</td>" +
                        "<td>" + res.rows[lengthKey].employee.birthday + "</td>" +
                        "<td>" + (res.rows[lengthKey].loginInfo.permission == 0 ? "普通用户" : res.rows[lengthKey].loginInfo.permission == 1 ? "管理员" : res.rows[lengthKey].loginInfo.permission == 2 ? "超级管理员" : "") + "</td>";
                    if (res.rows[lengthKey].loginInfo.permission != 2) {
                        html += "<td><button type=\"button\"  class=\"btn btn-success  updateinfo\" name=" + res.rows[lengthKey].loginInfo.empSn + "> <span class=\"glyphicon glyphicon-pencil \" aria-hidden=\"true\"></span>编辑</button><button type=\"button\" class=\"btn btn-danger deleteinfo   \" name=" + res.rows[lengthKey].loginInfo.empSn + ">  <span class=\"glyphicon glyphicon-trash\" aria-hidden=\"true\"></span>删除</button></td>" +
                            "</tr>"
                    }
                }
                $("#content_tabletr").html(html);
                totle = res.total;
                $(".pull-left").html("总共有<span> " + res.total + " </span>记录，当前页<span> " + pagenum + " /" + (Math.round(totle / PageSize) + 1) + "</span>页");
                $(".updateinfo").click(function () {
                        var empsn = $(this).attr("name");
                        $('.adduserinfo').modal();
                        $.ajax({
                            type: 'get',
                            url: getlogininfo + "?empSn=" + empsn,
                            success: function (res) {
                                $('.adduserinfo').modal();
                                //获取数据
                                $(".modal-title").text("修改");
                                $(".modal-body").html("");
                                var updateinfo = "  <div class=\"form-group\">" +
                                    " <label for=\"loginname\">名字</label>" +
                                    " <input type=\"text\" disabled class=\"form-control\" id=\"loginname\"    placeholder=\"名字\"  value=" + res.rows[0].employee.name + ">" +
                                    " </div>" +
                                    " <div class=\"form-group\">" +
                                    " <label for=\"loginemp\">工号</label>" +
                                    " <input type=\"text\" disabled class=\"form-control\" id=\"loginemp\"  placeholder=\"工号\" value=" + res.rows[0].employee.empSn + " >" +
                                    " </div>" +
                                    " <div class=\"form-group\">" +
                                    " <label for=\"loginpassword\">密码</label>" +
                                    " <input type=\"password\"  class=\"form-control\" id=\"loginpassword\" placeholder=\"密码\">" +
                                    "</div>" +
                                    " <div class=\"form-group\">" +
                                    "     <label for=\"birthday\">生日</label>" +
                                    "     <input type=\"date\"  class=\"form-control\" id=\"birthday\" placeholder=\"生日\" value=" + res.rows[0].employee.birthday + ">" +
                                    " </div>" +
                                    "<div class=\"form-group\">" +
                                    "<label for=\"loginsex\" >性别</label>" +
                                    "<select id=\"loginsex\" class=\"form-control\" disabled  value=" + res.rows[0].employee.sex + ">" +
                                    "<option value=\"男\">男</option>" +
                                    "<option value=\"女\">女</option>" +
                                    " </select>" +
                                    "</div>" +
                                    " <div class=\"form-group\">" +
                                    "<label for=\"loginphone\">手机号</label>" +
                                    "<input type=\"text\"  disabled class=\"form-control\" id=\"loginphone\" placeholder=\"手机号\" value=" + res.rows[0].employee.phone + "> " +
                                    " </div>" +
                                    "<div class=\"form-group\">" +
                                    "<label for=\"permission\">用户角色</label>" +
                                    "<select id=\"permission\" class=\"form-control\" >";

                                if (res.rows[0].loginInfo.permission == 0) {

                                    updateinfo += "<option value=\"0\" selected  =  \"selected\">普通用户</option>" +
                                        "<option value=\"1\">管理员</option>";
                                } else if (res.rows[0].loginInfo.permission == 1) {
                                    updateinfo += "<option value=\"0\" >普通用户</option>" +
                                        "<option value=\"1\" selected  =  \"selected\">管理员</option>";
                                }


                                updateinfo += "</select>" +
                                    "  </div>" +
                                    "<div class=\"form-group\">" +
                                    "  <label for=\"permission\">是否停用</label>" +
                                    "<select id=\"status\" class=\"form-control\" >";
                                if (res.rows[0].loginInfo.status == 0) {
                                    updateinfo += "<option value=\"0\" selected  =  \"selected\">可用</option>" +
                                        "<option value=\"1\">停用</option>";
                                } else if (res.rows[0].loginInfo.status == 1) {
                                    updateinfo += "<option value=\"0\" >可用</option>" +
                                        "<option value=\"1\"  selected= \"selected\" >停用</option>";
                                }
                                updateinfo += "</select>" +
                                    "   </div>";
                                $(".modal-body").html(updateinfo)
                                $("#btn_addInfo").text("保存")
                            }
                        })


                    }
                )
                $(".deleteinfo").click(function () {
                    var empsn = $(this).attr("name")

                    $.ajax({
                        url: '../../crud/info/'+empsn,
                        type: 'delete',
                        success: function (rs) {
                            //删除成功后的回调函数
//    					alert(rs);
                            if (rs.code == 200) {
                                SucceMsg("删除成功");



                            }
                        }
                    });


                })
            }

        }
    })
    ;

}


function getInfoForm() {

    var name = $("#loginname").val()
    var empSn = $("#loginemp").val()
    var password = $("#loginpassword").val()
    var sex = $("#loginsex").val()
    var phone = $("#loginphone").val()
    var status = $("#status").val()
    var permission = $("#permission").val()
    var birthday = $("#birthday").val()

    var data = {
        name: name,
        empSn: empSn,
        password: password,
        sex: sex,
        phone: phone,
        status: status,
        permission: permission,
        birthday: birthday

    }
    console.log(data)
    if ($("#btn_addInfo").text() == "保存") {
        $.ajax({
            url: '../../crud/info/updateinfo',
            type: 'post',
            contentType: "application/json;charset=utf-8",
            dataType: "json",
            data: JSON.stringify(data),
            success: function (rs) {
                //删除成功后的回调函数
//    					alert(rs);
                if (rs.code == 200) {
                    SucceMsg("保存成功");
                    $('.adduserinfo').modal('hide')


                }
            }
        });


    } else if ($("#btn_addInfo").text() == "新增") {
        $.ajax({
            url: '../../crud/info/addinfo',
            type: 'post',
            contentType: "application/json;charset=utf-8",
            dataType: "json",
            data: JSON.stringify(data),
            success: function (rs) {
                //删除成功后的回调函数
//    					alert(rs);
                if (rs.code == 200) {
                    SucceMsg("添加成功");
                    $('.adduserinfo').modal('hide')
                    getUsers();

                }
            }
        });
    }


}
