const getlogininfo = "../../crud/info/getlogininfo";
$(function () {

    $('.collapse_all').on('shown.bs.collapse', function () {
        var a = $(this).prev(); //获取到上一个兄弟节点的dom对象
        var i = a.children();
        i.removeClass('icon-jia1');
        i.addClass('icon-hengxian1');
    });

    $('.collapse_all').on('hidden.bs.collapse', function () {
        var a = $(this).prev(); //获取到上一个兄弟节点的dom对象
        var i = a.children();
        i.removeClass('icon-hengxian1');
        i.addClass('icon-jia1');
    })

    $(".collapse_all > li > a").click(function (el) {
        el.preventDefault();  //禁用a标签原本的功能
        var $this = $(this);
        $(".collapse_all > li > a").removeClass("navactive");
        $this.addClass("navactive");
        $("#iframe-contant").attr('src', $this.data("iframesrc"));
        var text = $this.text();
        var mnav = $this.parent().parent().prev().text();
        $("#path_nav > .breadcrumb > li:last-child").html(text);
        $("#path_nav > .breadcrumb > li:last-child").prev().html(mnav);
    });

    $.ajax({
        type: 'get',
        url: getlogininfo,
        success: function (res) {
            if (res.code == 500) {
                ErrorMsg(res.msg);
            } else {
                if (res.data.permission == 1) {
                    var HTML= '<span style="color: #09A0A5;font-size: 10px">普通用户：</span>';

                    $(".loginName").html(HTML+res.data.name);
                }else  if (res.data.permission == 0) {
                    var HTML= '<span style="color: #09A0A5;font-size: 10px">超级管理员：</span>';
                    $(".loginName").html(HTML+res.data.name);

                }else  if (res.data.permission == 2) {
                    var HTML= '<span style="color: #09A0A5;font-size: 10px">高级用户：</span>';
                    $(".loginName").html(HTML+res.data.name);

                }

            }

        }
    });


});
